$(document).ready(
    function(){

        $("#log-out").on('click', 
        function() {
            console.log("ok");
         }
        );

        $("#btn-log").click(
            function(){
                let var_nome =  $("#input-user").val();
                let var_psw = $("#input-psw").val();
                $.ajax(
                    {
                        url : "http://localhost:8080/HW_CHAT/verificalogin",
                        method : "POST",
                        data :
                         {
                            "input_username" : var_nome,
                            "input_password" : var_psw,
                        },
                        success : function(ris){
                                 if(ris=="UTENTE"){
                                window.location.href="http://localhost:8080/HW_CHAT/Chat.jsp";   
                                console.log(ris);
                            }
                            else if (ris=="ADMIN"){
                                window.location.href="http://localhost:8080/HW_CHAT/Amministratore.html";  
                            }
							
                        }
                    }
                );

            }
        );

        $("#btn-insert").click(
            function(){
                let ruolo = ($('input[type=radio]:checked').val()) ;
                let var_nome =  $("#input-user").val;
                let var_psw = $("#input-psw").val;
                $.ajax(
                    {
                        url : "localhost:8080/HW_CHAT/insert",
                        method : "POST",
                        data :
                         {
                            "input-user" : var_nome,
                            "input-psw" : var_psw,
                            "input-ruolo" : ruolo
                        },
                        success : function(ris){
                            console.log("ok");
                        }
                    }
                );
            }
        );
        
    }
);