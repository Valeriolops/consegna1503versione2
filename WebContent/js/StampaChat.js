function stampaChat(elenco_chat){
    let stringa = "";
    for (let i = 0; i < elenco_chat.length; i++) {
        let obj_chat = elenco_chat[i];
        stringa += "<div class=\"form-group row\">";
        stringa += "<label class=\"nickname\" class=\"col-sm-2 col-form-label\">"+obj_chat.nickname_chat+"</label>";
        stringa += "<div class=\"col-sm-10\">";
        stringa += "<textarea class=\"form-control\" id=\"exampleFormControlTextarea1\" rows=\"3\">"+obj_chat.testo+"</textarea>";
        stringa += "</div></div>";
    }

    $("#chat-area").html(stringa);
}

$(document).ready(
    function(){
        let threadInterval = window.setInterval(
            function(){
                $.ajax(
                    {
                        url : "http://localhost:8080/HW_CHAT/recuperamessaggi",
                        method : "POST",
                        success : function(risultato){
                            stampaChat(risultato);
                        }
                    }
        
                );  
            }
        , 1000);
        


        $("#btn-invio").click(
            function(){
                console.log($("#testo-invio").val());
                $.ajax(
                    {
                        url : "http://localhost:8080/HW_CHAT/inseriscimessaggio",
                        method : "POST",
                        data: {
                            input_testo : $("#testo-invio").val(),
                        },
                        success : function(risultato){
                            console.log("ok");
                            alert("messaggio inserito guarda in basso");
                            window.location.href="http://localhost:8080/HW_CHAT/Chat.jsp";
                        }
                    }
                );
            }
        );
    }

);