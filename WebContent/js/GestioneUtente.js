$(document).ready(
    function(){
        $("#btn-elimina").on("click",
            function(){
                console.log("clic elimina");
                $.ajax(
                    {
                        url : "http://localhost:8080/HW_CHAT/eliminautente",
                        method : "POST" , 
                        success : function(){
                        console.log("eliminato");
                            alert("Eliminazione avvenuta con successo!");
                                window.location.href="http://localhost:8080/HW_CHAT/index.html";  
                        }
                    }
                );
            }
        );

        $("#btn-insert").click(
            function(){
                $.ajax(
                    {
                        url : "http://localhost:8080/HW_CHAT/inseriscipersona",
                        method : "POST",
                        data : 
                        {
                             input_username : $("#input-user").val(),
                             input_password : $("#input-psw").val(),
                             input_nickname : $("#input-nick").val(),
                              input_ruolo :($('input[type=radio]:checked').val()) 
                        },
                        success: function(){
                            alert("Inserimento avvenuto con successo!");
                            window.location.href="http://localhost:8080/HW_CHAT/index.html";  
                        }
                    }
                );
            }
        );
    }
);