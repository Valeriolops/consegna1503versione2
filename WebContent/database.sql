DROP DATABASE IF EXISTS hw_chat;
CREATE DATABASE hw_chat;
USE hw_chat; 

create table persona (
personaId INTEGER NOT NULL AUTO_INCREMENT,
nickname  VARCHAR(150) NOT NULL UNIQUE,
ruolo VARCHAR(20) NOT NULL CONSTRAINT CHECK  (ruolo = "UTENTE" OR ruolo = "ADMIN") ,
username  VARCHAR(20)  NOT NULL UNIQUE,
psw VARCHAR(10)  NOT NULL UNIQUE,
PRIMARY KEY (personaId)
);


Select  personaId,nickname,ruolo,username,psw from persona Where username="sjore0" and psw="98AeGz8GeB";


insert into persona(nickname,ruolo,username,psw) values
("Gigi","ADMIN","Ghiglione","ABC132");

select * from persona;

create table chat (
messagioId INTEGER NOT NULL AUTO_INCREMENT,
testo text ,
orario datetime NOT NULL,
nickname_chat VARCHAR(150) NOT NULL ,
foreign key(nickname_chat) references persona(nickname), 
PRIMARY KEY (messagioId)
);

insert into persona (nickname, username, ruolo, psw) values ('Sabine', 'SAB', 'UTENTE', '1234');
insert into persona (nickname, username, ruolo, psw) values ('Irita', 'ilesmonde1', 'UTENTE', '4g5aV54YoY');
insert into persona (nickname, username, ruolo, psw) values ('Ninon', 'ncowper2', 'UTENTE', 'xyT1AqlGmT');
insert into persona (nickname, username, ruolo, psw) values ('Phedra', 'pholburn3', 'UTENTE', 'vvSsMp1JoK');
insert into persona (nickname, username, ruolo, psw) values ('Gerick', 'gabramovic4', 'UTENTE', 'mmBrXrs24W');
insert into persona (nickname, username, ruolo, psw) values ('Marji', 'mdufray5', 'UTENTE', 'rm1fPcnTmH');
insert into persona (nickname, username, ruolo, psw) values ('Sinclare', 'smaccoughend', 'UTENTE', 'rjXg8t8SdM');
insert into persona (nickname, username, ruolo, psw) values ('Scarface', 'sscardefielde', 'UTENTE', 'euEd0voKoT');

-- delete from persona where nickname="Scarface";
select * from persona;

insert into chat (testo, orario, nickname_chat) values ('felis ut at dolor quis odio consequat varius integer ac leo', '2021-01-30 00:05:24', 'Sabine');
insert into chat (testo, orario, nickname_chat) values ('vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus', '2020-04-02 15:31:01', 'Irita');
insert into chat (testo, orario, nickname_chat) values ('arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque', '2021-02-15 20:12:40', 'Sabine');
insert into chat (testo, orario, nickname_chat) values ('in felis donec semper sapien a libero nam dui proin', '2020-11-14 03:31:29', 'Irita');
insert into chat (testo, orario, nickname_chat) values ('morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus', '2020-08-13 23:00:59', 'Sabine');
insert into chat (testo, orario, nickname_chat) values ('pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet', '2020-08-30 07:45:01', 'Irita');
insert into chat (testo, orario, nickname_chat) values ('nulla ultrices aliquet maecenas leo odio condimentum id luctus nec', '2020-05-17 06:24:01', 'Sabine');
insert into chat (testo, orario, nickname_chat) values ('platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer', '2020-07-02 15:27:10', 'Irita');
insert into chat (testo, orario, nickname_chat) values ('luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec', '2021-02-02 08:10:01', 'Sabine');
insert into chat (testo, orario, nickname_chat) values ('at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium', '2020-04-01 00:18:07', 'Irita');







select * from chat;

select    chat.messagioId, chat.testo, chat.orario, chat.nickname_chat
from persona,chat
where persona.nickname = chat.nickname_chat
order by chat.orario asc; 






