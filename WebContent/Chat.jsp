<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="Stile/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<title>Iscrizione</title>
</head>
<body>
               		<%
                    
                    	 HttpSession sessione = request.getSession();
                		 String ruolo_persona = (String)sessione.getAttribute("ruolo") != null ? (String)sessione.getAttribute("ruolo") : "";
                		// String nickUtente =(String) sessione.getAttribute("nickname");
                    %>

<div class="container-fluid">
<!-- QUI CI METTO LA TEXT AREA PER SCRIVERE -->
    <div class="row" style="margin-top: 50px">
        <div class="col">
            <div class="form-group">
                <textarea class="form-control" id="testo-invio" rows="3"></textarea>
                <button style="margin-top: 20px" id="btn-invio" type="button" class="btn btn-block btn-outline-success">INVIA!</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10" id="chat-area">
		</div>
		<div class="col-md-1"></div>
	</div>

</div>

<div class="container">
    <div class="row">
        <div class="col">
            <p>Vuoi eliminare il tuo account ? 
                <button id="btn-elimina" class="button" type="button">Clicca Qui</button>
            </p>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="js/StampaChat.js"></script>
<script src ="js/GestioneUtente.js"></script>
  </body>
</html>