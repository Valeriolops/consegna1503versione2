package progettoChat.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.fabric.Response;
import com.progettoChatClassi.Persona;

import progettoChat.dao.PersonaDAO;



@WebServlet("/verificalogin")
public class VerificaLogin extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("errore.jsp?tipo_errore=NOGET");
	}

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
//		response.setHeader("Access-Control-Allow-Origin", "http://localhost:8081");
//        response.setHeader("Access-Control-Allow-Methods", "POST");
		String user = request.getParameter("input_username") != null ? request.getParameter("input_username") : "";
		String pass = request.getParameter("input_password") != null ? request.getParameter("input_password") : "";

		Persona pTemp = new Persona();
		pTemp.setUsername(user);
		pTemp.setPsw(pass);
		

		PersonaDAO pDao = new PersonaDAO();

		try {
			pTemp = pDao.checkCredentials(pTemp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (pTemp.getPersonaId() != null) {

				HttpSession sessione = request.getSession();

				if (pTemp.getRuolo().equals("ADMIN")) {
					sessione.setAttribute("role", "ADMIN");
					sessione.setAttribute("nickname", pTemp.getNickname());
					PrintWriter out = response.getWriter();
					out.print("ADMIN");
					
					
					return;
				}

				if (pTemp.getRuolo().equals("UTENTE")) {
					sessione.setAttribute("role", "UTENTE");
					sessione.setAttribute("nickname", pTemp.getNickname());
					PrintWriter out = response.getWriter();
					out.print("UTENTE");
					return;
				}

				response.sendRedirect("errore.jsp?tipo_errore=NOROLE");
			} else {
				response.sendRedirect("errore.jsp?tipo_errore=NOUSER");
			}
		}

	}
}
