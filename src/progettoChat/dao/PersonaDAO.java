package progettoChat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.progettoChat.connessione.ConnettoreDB;
import com.progettoChatClassi.Persona;

public class PersonaDAO implements Dao<Persona> {

	
	public Persona checkCredentials(Persona objPersona) throws SQLException {
		
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT personaId,nickname,ruolo,username,psw from persona Where username = ? AND psw = ? ";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, objPersona.getUsername());
       	ps.setString(2, objPersona.getPsw());
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();
       	
       	objPersona.setPersonaId(risultato.getInt(1));
       	objPersona.setNickname(risultato.getString(2));
       	objPersona.setRuolo(risultato.getString(3));
       	
       	return objPersona;
       	
		
	}
	
	
	
	
	public Persona getPersonaByNickName(String nick) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = " Select  personaId,nickname,ruolo,username,psw from persona Where nickname= ? ";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, nick);
		

		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Persona pTemp = new Persona();
		pTemp.setPersonaId(risultato.getInt(1));
		pTemp.setNickname(risultato.getString(2));
		pTemp.setRuolo(risultato.getString(3));
		pTemp.setUsername(risultato.getString(4));
		pTemp.setPsw(risultato.getString(5));

		return pTemp;
	} 
	
	
	
	
	
	
	public Persona getPersonaByUsernamePassword(String username, String password) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = " Select  personaId,nickname,ruolo,username,psw from persona Where username= ?  and psw= ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, username);
		ps.setString(2, password);

		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Persona pTemp = new Persona();
		pTemp.setPersonaId(risultato.getInt(1));
		pTemp.setNickname(risultato.getString(2));
		pTemp.setRuolo(risultato.getString(3));
		pTemp.setUsername(risultato.getString(4));
		pTemp.setPsw(risultato.getString(5));

		return pTemp;
	}

	@Override
	public Persona getById(int id) throws SQLException {

		return null;
	}

	@Override
	public ArrayList<Persona> getAll() throws SQLException {
		ArrayList<Persona> elenco = new ArrayList<Persona>();

		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = " Select  personaId,nickname,ruolo,username,psw from persona ";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();

		while (risultato.next()) {
			Persona pTemp = new Persona();
			pTemp.setPersonaId(risultato.getInt(1));
			pTemp.setNickname(risultato.getString(2));
			pTemp.setRuolo(risultato.getString(3));
			pTemp.setUsername(risultato.getString(4));
			pTemp.setPsw(risultato.getString(5));
			elenco.add(pTemp);

		}

		return elenco;
	}

	@Override
	public void insert(Persona t) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "insert into persona(nickname,ruolo,username,psw) values (? ,?,?,?)";
			
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getNickname());
       	ps.setString(2, t.getRuolo());
       	ps.setString(3, t.getUsername());
       	ps.setString(4, t.getPsw());
    	ps.executeUpdate();
    	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
       	t.setPersonaId(risultato.getInt(1));
		

	}

	@Override
	public boolean delete(Persona t) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query ="delete from persona where nickname= ? ";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getNickname());
		
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
       	return false;
	}

	@Override
	public boolean update(Persona t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
