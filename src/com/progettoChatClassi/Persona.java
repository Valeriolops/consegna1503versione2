package com.progettoChatClassi;

public class Persona {
	
	
	private Integer personaId;
	private String nickname;
	private String ruolo;
	private String username;
	private String psw;
	
	
	
	
	public Persona() {
		
	}




	public Integer getPersonaId() {
		return personaId;
	}




	public void setPersonaId(Integer personaId) {
		this.personaId = personaId;
	}




	public String getNickname() {
		return nickname;
	}




	public void setNickname(String nickname) {
		this.nickname = nickname;
	}




	public String getRuolo() {
		return ruolo;
	}




	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}




	public String getUsername() {
		return username;
	}




	public void setUsername(String username) {
		this.username = username;
	}




	public String getPsw() {
		return psw;
	}




	public void setPsw(String psw) {
		this.psw = psw;
	}




	@Override
	public String toString() {
		return "Persona [personaId=" + personaId + ", nickname=" + nickname + ", ruolo=" + ruolo + ", username="
				+ username + ", psw=" + psw + "]";
	}
	
	
	
	
	
	

}
