package com.progettoChat.API;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.progettoChatClassi.Persona;

import progettoChat.dao.PersonaDAO;


/**
 * Servlet implementation class EliminaUtente
 */
@WebServlet("/eliminautente")
public class EliminaUtente extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		response.setHeader("Access-Control-Allow-Origin", "http://localhost:8081");
//        response.setHeader("Access-Control-Allow-Methods", "POST");
        
		HttpSession sessione = request.getSession();
		String nickname = (String) sessione.getAttribute("nickname");
		
		PersonaDAO pDAO = new PersonaDAO();
		Persona pDaElimare = new Persona();
		try {
			pDaElimare=pDAO.getPersonaByNickName(nickname);
			pDAO.delete(pDaElimare);
			System.out.println("Ho eliminato la persona "+pDaElimare.toString());
			sessione.invalidate();
			
			
			
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		
		
	
		
	
	}

}
