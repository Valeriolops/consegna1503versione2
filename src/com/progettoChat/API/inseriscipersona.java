package com.progettoChat.API;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.progettoChatClassi.Persona;
import com.progettoChatClassi.ResponsoOperazione;

import progettoChat.dao.PersonaDAO;

/**
 * Servlet implementation class inseriscipersona
 */
@WebServlet("/inseriscipersona")
public class inseriscipersona extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		response.setHeader("Access-Control-Allow-Origin", "http://localhost:8081");
//        response.setHeader("Access-Control-Allow-Methods", "POST");
		
		String varNickName =  request.getParameter("input_nickname");
		String varRuolo = request.getParameter("input_ruolo");
		String varUsername = request.getParameter("input_username");
		String varPassword = request.getParameter("input_password");
		Persona pTemp = new Persona();
		pTemp.setRuolo(varRuolo);
		pTemp.setNickname(varNickName);
		pTemp.setUsername(varUsername);
		pTemp.setPsw(varPassword);
		String strErrore = "";
		PersonaDAO pDAO = new PersonaDAO();
		try {
			pDAO.insert(pTemp);
			System.out.println("Utente inserito");
		} catch (SQLException e) {
			e.printStackTrace();
			strErrore = e.getMessage();
		}
		
		
		PrintWriter out = response.getWriter();
		Gson jsonizzatore = new Gson();
		if(pTemp.getPersonaId()== null) {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", strErrore);
			out.print(jsonizzatore.toJson(res));
			
		}
		else {
			ResponsoOperazione res = new ResponsoOperazione("OK", "");
			out.print(jsonizzatore.toJson(res));
		}
		
		
		
		//input_username
		//input_
		
		
		
	}

}
