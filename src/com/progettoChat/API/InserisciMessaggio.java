package com.progettoChat.API;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.progettoChatClassi.Chat;
import com.progettoChatClassi.ResponsoOperazione;

import progettoChat.dao.ChatDAO;

/**
 * Servlet implementation class InserisciMessaggio
 */
@WebServlet("/inseriscimessaggio")
public class InserisciMessaggio extends HttpServlet {
	
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
//		response.setHeader("Access-Control-Allow-Origin", "http://localhost:8081");
//        response.setHeader("Access-Control-Allow-Methods", "POST");
//		
		
		HttpSession sessione = request.getSession();
		
		
		String nickname = (String) sessione.getAttribute("nickname");
		
		
		System.out.println(nickname);
		String testo = request.getParameter("input_testo");
		System.out.println(testo);
		Date a = new Date();
		System.out.println(a);
		Chat cTemp = new Chat();
		cTemp.setNickname_chat(nickname);
		cTemp.setTesto(testo);
		cTemp.setOrario(a);
		ChatDAO cDao = new ChatDAO();
		String strErrore = "";
		
		try {
			cDao.insert(cTemp);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			strErrore = e.getMessage();
		}
		
		
		PrintWriter out = response.getWriter();
		Gson jsonizzatore = new Gson();
		
		if(cTemp.getMessagioId()!= null) {
			ResponsoOperazione res = new ResponsoOperazione("OK", "");
			out.print(jsonizzatore.toJson(res));
			
			
		}else {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", strErrore);
			out.print(jsonizzatore.toJson(res));
		}
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
