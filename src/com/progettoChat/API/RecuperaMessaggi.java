package com.progettoChat.API;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.progettoChatClassi.Chat;
import com.progettoChatClassi.ResponsoOperazione;

import progettoChat.dao.ChatDAO;

/**
 * Servlet implementation class RecuperaMessaggi
 */
@WebServlet("/recuperamessaggi")
public class RecuperaMessaggi extends HttpServlet {
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
//		response.setHeader("Access-Control-Allow-Origin", "http://localhost:8081");
//        response.setHeader("Access-Control-Allow-Methods", "POST");
		
	
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		ChatDAO cDao = new ChatDAO();
		ArrayList<Chat> elencoMessaggiChat = new ArrayList();
		try {
			elencoMessaggiChat = cDao.getAll();
			String risultatoJson = new Gson().toJson(elencoMessaggiChat);
			
			out.print(risultatoJson);

		} catch (SQLException e) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));	
			System.out.println(e.getMessage());
		} 
	
	
	}

}
